package ru.t1.ktitov.tm;

import ru.t1.ktitov.tm.constant.ArgumentConst;
import ru.t1.ktitov.tm.constant.TerminalConst;
import ru.t1.ktitov.tm.model.Command;
import ru.t1.ktitov.tm.util.FormatUtil;

import java.util.Scanner;


public final class Application {

    public static void main(String[] args) {
        if (processArguments(args)) System.exit(0);
        processCommands();
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter command: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArgument(final String argument) {
        if (argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            default:
                showErrorArgument();
        }
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private static void exit() {
        System.exit(0);
    }

    public static void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory: " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    public static void showErrorArgument() {
        System.err.println("Error! Not supported argument");
        System.out.println();
    }

    public static void showErrorCommand() {
        System.err.println("Error! Not supported command");
        System.out.println();
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Titov Kirill");
        System.out.println("E-mail: kir.titoff@gmail.com");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

}
